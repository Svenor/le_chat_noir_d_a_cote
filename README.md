Le chat noir d'à côté
=====================

Informations
------------
Vous pouvez trouver ce projet sur [Gitlab](https://gitlab.com/Svenor/le_chat_noir_d_a_cote).

Sur ce repository, vous trouverez une version jouable sur navigateur (`WebGL_Build`), ainsi que le code source (`Unity Project`).

Le jeu
------
`Le chat noir d'à côté` est un platformer 2D dans lequel on joue un petit renard, à la recherche de sa mémoire qu'il a perdu. Je me suis servi du `Sunny Land Asset Pack` pour toute les sprites. J'ai aussi utilisé un script récupéré sur Internet, me permettant d'avoir un composant `CharacterController2D`, que j'ai modifié et adapté à mes besoins.

Je me suis inspiré de la nouvelle `Le chat noir`, d'Edgar Allan Poe (que je n'ai pas encore lu) pour l'histoire et le logo du jeu. Il est donc plus que possible que l'histoire ne corresponde absolument pas à la nouvelle, j'ai cependant mis les premières phrases de la nouvelle dans mon jeu.

Les problèmes que j'ai rencontré (et leurs solutions)
-----------------------------------------------------
J'ai rencontré certains problèmes durant la réalisation de ce jeu.
Tout d'abord, j'ai du mettre en place des plateformes, et que mon personnage ne puisse pas les traverser. J'ai fait des essais en supprimant la gravité quand les deux Collider entrent en contact, mais il restait certains problèmes, comme le fait que le personnage ne tombe pas alors qu'il touchait une plateforme avec sa tête. J'ai donc pensé à utiliser plusieurs Colliders pour le Player, avant de tomber sur le script `CharacterController2D`, qui gérait toute cette partie physique.

Un autre problème rencontré a été que mon personnage traversait les tuiles. La solution, très simple, a été de vérifier si les colliders du Player étaient de la bonne taille (ils ne l'étaient pas), et de les rectifier.

Enfin, mon dernier problème, qui vraisemblablement vient d'Unity, est un problème visuel. En effet, de longues bandes blanches verticales traversent l'écran de jeu de temps en temps, et je n'ai trouvé sur Internet aucune solution. Il y avait cependant quelques fois où ces bandes n'apparaissaient pas quand je lançais le jeu, sans savoir pourquoi.

Les appareils sur lesquels j'ai testé mon jeu
---------------------------------------------
J'ai fais un Build de mon jeu sur WebGL, que j'ai ensuite testé sur Firefox, sur Windows et Ubuntu.

<i>Lucas Grospart</i>
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour {
    public float runSpeed = 40f;

	private float horizontalMove = 0f;
	private bool jump = false;
    private bool grounded = true;

    private Vector3 leftCameraBorder;
    private Vector3 rigthCameraBorder;
    public CharacterController2D controller;
	public Animator animator;
    public AudioClip footstepsSounds;
    

    void Start() {
        leftCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3 (0, 0, 0));
        rigthCameraBorder = Camera.main.ViewportToWorldPoint(new Vector3 (1, 1, 0));

        if (SceneManager.GetActiveScene().name == "game-scene1") {
            gameObject.transform.position = new Vector3(PlayerPrefs.GetFloat("playerPositionX"), PlayerPrefs.GetFloat("playerPositionY"), 0);
        }
    }

    void Update() {
        if (PlayerPrefs.GetString("dialogbox", "close") == "close") {
            horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

            animator.SetFloat("speed", Mathf.Abs(horizontalMove));

            if (Input.GetButtonDown("Jump")) {
                grounded = false;
                jump = true;
                animator.SetBool("isJumping", true);
            }
        }

        // Bruits de pas si la vitesse du joueur est supérieure à 0.2 et si le joueur touche une tuile
        if (Mathf.Abs(horizontalMove) >= 0.01 && grounded) {
            AudioSource footsteps = gameObject.GetComponent<AudioSource>();
            if (!footsteps.isPlaying){
                footsteps.clip = footstepsSounds;
                footsteps.Play();
            }
        }
        PlayerPrefs.SetFloat("playerX", gameObject.transform.position.x);
    }

    public void onLanding() {
        grounded = true;
        animator.SetBool("isJumping", false);
    }

    void FixedUpdate() {
        if (gameObject.transform.position.x < -8.5 && horizontalMove < 0) {
            horizontalMove = 0;
            // On interdit au joueur d'aller à gauche de la map
        }

        if (PlayerPrefs.GetString("dialogbox", "open") == "open") {
            horizontalMove = 0;
            // On stop tout déplacement du joueur si une dialogbox est ouverte
        }
        // Move the character
        controller.Move(horizontalMove * Time.fixedDeltaTime, jump);
        jump = false;
    }
}

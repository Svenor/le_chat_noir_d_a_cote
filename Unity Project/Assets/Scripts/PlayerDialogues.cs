﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerDialogues : MonoBehaviour {
    public DialogueTrigger initDialogue;
    private bool initDialogueTriggered;
    public DialogueTrigger initDialogueHouse;
    private bool initDialogueHouseTriggered;
    public DialogueTrigger dialogue1;
    private bool dialogue1Triggered;
    public DialogueTrigger dialogueFall;


    void Update() {
        initDialogueTriggered = PlayerPrefs.GetString("initDialogueTriggered", "false") == "true";
        dialogue1Triggered = PlayerPrefs.GetString("dialogue1Triggered", "false") == "true";
        initDialogueHouseTriggered = PlayerPrefs.GetString("initDialogueHouseTriggered", "false") == "true";
        if (! initDialogueTriggered) {
            initDialogue.TriggerDialogue();
            PlayerPrefs.SetString("initDialogueTriggered", "true");
        }
    
        if (! initDialogueHouseTriggered && SceneManager.GetActiveScene().name == "game-house") {
            initDialogueHouse.TriggerDialogue();
            PlayerPrefs.SetString("initDialogueHouseTriggered", "true");
        }

        if (!dialogue1Triggered && gameObject.transform.position.x > 8) {
            dialogue1.TriggerDialogue();
            PlayerPrefs.SetString("dialogue1Triggered", "true");
        }

        if (gameObject.transform.position.y <= -10) {
            // On téléporte le joueur à côté de la crevasse s'il tombe dedans
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
            gameObject.transform.position = new Vector3(60, 1, 0);
            gameObject.GetComponent<Rigidbody2D>().gravityScale = 3;
            dialogueFall.TriggerDialogue(); // On affiche un dialogue au joueur
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitialisationScript : MonoBehaviour {
    void Update() {
        Invoke("goMenu", 2.0f);
    }

    void goMenu() {
        SceneManager.LoadScene("menu");
    }
}

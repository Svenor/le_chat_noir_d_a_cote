﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour {
    private Singleton instance = null; // Initialisation du Singleton à null

    void Awake() {
        if (instance == null) {
            instance = this;
        }
        else {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return this;
    }
}

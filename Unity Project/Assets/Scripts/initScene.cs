﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class initScene : MonoBehaviour {
    void Awake() {
        init();
    }

    public void init(){
        // On initialise toutes les variables globales PlayerPrefs
        PlayerPrefs.SetString("hasKeyHouse", "false");
        PlayerPrefs.SetString("hasKeyFinalDoor", "false");
        PlayerPrefs.SetString("initDialogueTriggered", "false");
        PlayerPrefs.SetString("initDialogueHouseTriggered", "false");
        PlayerPrefs.SetString("dialogue1Triggered", "false");
        PlayerPrefs.SetString("endGame", "false");

        PlayerPrefs.SetFloat("playerPositionX", -3);
        PlayerPrefs.SetFloat("playerPositionY", -2);
    }
}

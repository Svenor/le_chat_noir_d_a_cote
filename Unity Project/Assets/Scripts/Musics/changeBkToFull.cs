﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeBkToFull : MonoBehaviour {
    private AudioClip fullMusic;
    public void Music() {
        fullMusic = (AudioClip)Resources.Load("Sounds/full", typeof(AudioClip));
        AudioSource bkAudio = gameObject.GetComponent<AudioSource>();
        bkAudio.clip = fullMusic;
        bkAudio.loop = true;
        bkAudio.Play();
    }
}

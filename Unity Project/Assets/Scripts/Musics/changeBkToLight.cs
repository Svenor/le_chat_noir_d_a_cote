﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class changeBkToLight : MonoBehaviour {
    private AudioClip lightMusic;
    public void Music() {
        lightMusic = (AudioClip)Resources.Load("Sounds/light", typeof(AudioClip));
        AudioSource bkAudio = gameObject.GetComponent<AudioSource>();
        bkAudio.clip = lightMusic;
        bkAudio.loop = true;
        bkAudio.Play();
    }
}

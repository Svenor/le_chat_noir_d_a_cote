﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {
    public Dialogue dialogue;

    public void TriggerDialogue() {
        // On lance le dialogue avec les input
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DialogueManager : MonoBehaviour {

    public Text nameText;
    public Text dialogueText;
    public Animator animator;
    private Queue<Tuple<string, string>> lines;
    void Start() {
        lines = new Queue<Tuple<string, string>>();
    }

    public void StartDialogue(Dialogue dialogue) {
        // On lance le dialogue
        animator.SetBool("isOpen", true); // On dit à l'animator d'ouvrir la dialogbox
        PlayerPrefs.SetString("dialogbox", "open"); 
        // annonce que la dialogbox est ouverte
        // (pour stopper tout déplacement du joueur pendant qu'elle est ouverte)
        lines.Clear();
        foreach (Tuple<string, string> line in dialogue.getDialogue()) {
            // On parcourt les input pour peupler la liste phrases et de personnages
            lines.Enqueue(line);
        }

        DisplayNextSentence(); // On affiche la première phrase
    }

    public void DisplayNextSentence() {
        if (lines.Count == 0) {
            // Si la liste de dialogues est vide, on termine le dialogue
            EndDialogue();
            return;
        }

        Tuple<string, string> line = lines.Dequeue(); // On prend le prochain dialogue et on l'enlève de la liste
        nameText.text = line.Item1; // On affiche le nom du parleur
        StopAllCoroutines();
        StartCoroutine(TypeSentence(line.Item2)); // On lance la Coroutine TypeSentence
    }

    IEnumerator TypeSentence (string sentence) {
        // On affiche la phrase lettre par lettre avec un délai
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray()) {
            dialogueText.text += letter;
            yield return 2;
        }
    }

    void EndDialogue() {
        // On termine le dialogue
        animator.SetBool("isOpen", false);
        PlayerPrefs.SetString("dialogbox", "close");
        if (PlayerPrefs.GetString("endGame") == "true") {
            // Si c'était le dialogue de fin du jeu, on affiche la scène de fin
            SceneManager.LoadScene("end-scene");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Animator animator;
    private float playerX;

    void Update() {
        // On déplace la caméra selon la position du joueur
        playerX = PlayerPrefs.GetFloat("playerX");
        animator.SetFloat("playerX", playerX);
    }
}

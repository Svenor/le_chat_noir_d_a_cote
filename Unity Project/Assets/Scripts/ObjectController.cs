﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectController : MonoBehaviour{
    public string mode;
    private bool hasKeyHouse;
    private bool hasKeyFinalDoor;
    public DialogueTrigger dialogue;
    public GameObject e;
    public DialogueTrigger dialogueWKey;
    private bool isColliding = false;

    void Start() {
        if (mode != "finaldoor" && mode != "housedoor") {
            dialogueWKey = null;
        }
        e.SetActive(false);
        hasKeyHouse = false;
        hasKeyFinalDoor = false;
    }

    void Update() {
        hasKeyHouse = PlayerPrefs.GetString("hasKeyHouse", "false") == "true";
        hasKeyFinalDoor = PlayerPrefs.GetString("hasKeyFinalDoor", "false") == "true";
        if (isColliding) {
            if (Input.GetKeyDown(KeyCode.E)) {
                if (mode == "object") {
                    // S'il interagit avec un object
                    dialogue.TriggerDialogue();
                } else if (mode == "housedoor") {
                    if (hasKeyHouse) {
                        // Si le joueur a la clef de la porte de la maison
                        SceneManager.LoadScene("game-house");
                    } else {   
                        // S'il n'a pas la clef de la porte de la maison
                        dialogueWKey.TriggerDialogue();
                    }
                } else if (mode == "finaldoor") {
                    if (hasKeyFinalDoor) {
                        // S'il a la clef de la porte finale
                        dialogue.TriggerDialogue();
                        PlayerPrefs.SetString("endGame", "true");
                    } else {
                        // S'il n'a pas la clef de la porte finale
                        dialogueWKey.TriggerDialogue();
                    }
                } else if (mode == "rock") {
                    // S'il interagit avec le rocher
                    dialogue.TriggerDialogue();
                    PlayerPrefs.SetString("hasKeyHouse", "true");
                } else if (mode == "finalmemorystone") {
                    // S'il interagit avec la dernière pierre de mémoire
                    dialogue.TriggerDialogue();
                } else if (mode == "book") {
                    // S'il interagit avec le livre
                    dialogue.TriggerDialogue();
                    PlayerPrefs.SetString("hasKeyFinalDoor", "true");
                } else if (mode == "insidedoor") {
                    PlayerPrefs.SetFloat("playerPositionX", 19);
                    PlayerPrefs.SetFloat("playerPositionY", -3);
                    SceneManager.LoadScene("game-scene1");
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.tag == "Player") {
            if (mode == "rock") {
                isColliding = true;
            } else {
                isColliding = true;
                e.SetActive(true);
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider) {
        if (collider.tag == "Player") {
            isColliding = false;
            e.SetActive(false);
        }
    }
}

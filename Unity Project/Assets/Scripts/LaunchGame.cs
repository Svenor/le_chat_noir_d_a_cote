﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LaunchGame : MonoBehaviour {
    public void launchGame() {
        FindObjectOfType<Singleton>().GetComponent<changeBkToLight>().Music();
        // On lance le jeu
        SceneManager.LoadScene("game-scene1");
    }
}
